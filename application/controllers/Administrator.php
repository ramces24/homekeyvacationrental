<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller {
	public function index()
	{
		$this->admin_content('');
	}
	public function admin_content($view = "s",$data = null)
	{
		if(!empty($view)){
			$this->load->view('includes/admin-header',$data);
			$this->load->view($view,$data);
			$this->load->view('includes/admin-footer',$data);
		}
		else{
			echo "ari mo sod";
		}
	}
}
